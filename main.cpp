#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QQuickWindow>
#include <QQuickStyle>
#include <QSettings>
#include <QQuickItem>

#include "firsttabmodel.h"
#include "imageholder.h"

int main(int argc, char *argv[])
{
    qRegisterMetaType<Data>("Data");
    qRegisterMetaTypeStreamOperators<Data>("Data");
    qRegisterMetaTypeStreamOperators<QList<QString> >("QList<QString>");

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    QSettings settings("app.ini", QSettings::IniFormat);

    FirstTabModel model(&settings);
    ImageHolder holder(&settings);

    QQmlApplicationEngine engine;
    QQuickStyle::setStyle("Material");
    engine.rootContext()->setContextProperty("theModel", &model);
    engine.rootContext()->setContextProperty("images", &holder);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    qputenv("QT_QUICK_CONTROLS_STYLE", "material");
    QObject *rootObject = engine.rootObjects().first();

    QObject *dialog = rootObject->findChild<QObject*>("dialog");

    QObject::connect(dialog, SIGNAL(addRecord(const QString&, const QString&, const QString&, const QString&,
                                     const QString&, const QString&, const QString&)),
            &model, SLOT(addData(const QString&, const QString&, const QString&, const QString&,
                                const QString&, const QString&, const QString&)));

    return app.exec();
}
