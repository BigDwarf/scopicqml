#include "firsttabmodel.h"

QDataStream& operator<<(QDataStream& out, const Data& v) {
    out << v.valueInt << v.valueDouble << v.valueStr
        << v.valueInt_2 << v.valueDouble_2 << v.valueStr_2
        << v.valueStr_3;
    return out;
}

QDataStream& operator>>(QDataStream& in, Data& v) {
    in >> v.valueInt;
    in >> v.valueDouble;
    in >> v.valueStr;
    in >> v.valueInt_2;
    in >> v.valueDouble_2;
    in >> v.valueStr_2;
    in >> v.valueStr_3;
    return in;
}

FirstTabModel::FirstTabModel(QSettings *settings): mSettings(settings)
{
    int count = mSettings->value("RowCount", 0).toInt();
    for(int i = 0; i < count; ++i)
    {
        QVariant variant = mSettings->value("row" + QString::number(i));
        mData << variant.value<Data>();
    }
}

FirstTabModel::~FirstTabModel()
{
    mSettings->setValue("RowCount", mData.size());

    for (int i = 0; i < mData.size(); ++i)
    {
        mSettings->setValue("row" + QString::number(i), QVariant::fromValue(mData[i]));
    }
}

int FirstTabModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return mData.size();
}

int FirstTabModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return ROLES_COUNT;
}

QVariant FirstTabModel::data(const QModelIndex &index, int role) const
{
    if(index.column() < 0 ||
            columnCount() <= index.column() ||
            index.row() < 0 ||
            rowCount() <= index.row())
    {
        return QVariant();
    }

    auto data = mData[index.row()];
    switch(role)
    {
        case FIRST:
            return data.valueInt;
        case SECOND:
            return  data.valueDouble;
        case THIRD:
            return  data.valueStr;
        case FOURTH:
            return data.valueInt_2;
        case FIFTH:
            return data.valueDouble_2;
        case SIXTH:
            return data.valueStr_2;
        case SEVENTH:
            return data.valueStr_3;
        default:
            return QVariant();
    }
}

QHash<int, QByteArray> FirstTabModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[FIRST] = "first";
    roles[SECOND] = "second";
    roles[THIRD] = "third";
    roles[FOURTH] = "fourth";
    roles[FIFTH] = "fifth";
    roles[SIXTH] = "sixth";
    roles[SEVENTH] = "seventh";
    return roles;
}

void FirstTabModel::addData(const QString &first, const QString &second, const QString &third, const QString &fourth, const QString &fifth, const QString &sixth, const QString &seventh)
{
    beginResetModel();
    mData << Data {first.toInt(),second.toDouble(),third,
                   fourth.toInt(),fifth.toDouble(),
                    sixth, seventh };
    endResetModel();
}
