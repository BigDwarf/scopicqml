import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Material 2.0
import QtQuick.Controls 2.4
import Qt.labs.settings 1.0
import QtQuick 2.2
import QtQuick.Dialogs 1.1

ApplicationWindow {
    id: app
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Settings {
        property alias x: app.x
        property alias y: app.y
        property alias width: app.width
        property alias height: app.height
    }

    signal clickIndexSignal(var i)
    signal resetToDefaults ()
    onClickIndexSignal:{ // SIGNAL handler
        Material.theme = i !== 0 ?  Material.Dark : Material.Light
    }
    TabView{
        id: tableView
        objectName: "tabView"
        width: parent.width
        height: parent.height
        style: TabViewStyle {
            tabsMovable: true
        }

        Tab{
            id:first
            title: "First"
            height: parent.height
            TableView {
                sortIndicatorVisible: true
                model: theModel
                anchors.fill: parent

                TableViewColumn {title: "1"; role: "first"; width: 70 }
                TableViewColumn {title: "2"; role: "second"; width: 70   }
                TableViewColumn {title: "3"; role: "third"; width: 70 }
                TableViewColumn {title: "4"; role: "fourth"; width: 70 }
                TableViewColumn {title: "5"; role: "fifth"; width: 70 }
                TableViewColumn {title: "6"; role: "sixth"; width: 70 }
                TableViewColumn {title: "7"; role: "seventh"; width: 70 }
                Button
                {
                    id: myButton
                    y: tableView.height - 100
                    x: first.width/2 - width/2
                    width: tableView.width/5
                    height: 50
                    text: "Add record"
                    onClicked: dateDialog.visible = true
                }
            }
        }
        Tab{
            title: "Second"
            objectName: "secondTab"
            signal add()
            signal remove()
            MouseArea {
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                height: parent.height
                width: parent.width
                onClicked: {
                   if(mouse.button & Qt.RightButton) {
                       contextMenu.open()
                   }
               }
                Menu {
                    id: contextMenu
                    MenuItem {
                        text: "Add"
                        onTriggered: addImageDialog.open()

                    }
                    MenuItem {
                        text: "remove"
                        onTriggered: imageViewer.source = images.removeImage()
                    }
                }
                RowLayout{
                    anchors.fill: parent
                    spacing: 0
                    Dialog{
                        id: addImageDialog
                        objectName: "addImageDialog"
                        title: "Paste image path"
                        standardButtons: StandardButton.Ok | StandardButton.Cancel
                        onAccepted:
                        {
                            imageViewer.source = images.addImage(imagePathTextField.text)
                        }
                        TextField {
                            id: imagePathTextField
                            width: parent.width
                            height: 60
                        }
                    }
                    Button{
                        text: "Previous"
                        Layout.preferredWidth: 100
                        Layout.preferredHeight: 100
                        onClicked:  imageViewer.source = images.previous()
                    }
                    Image {
                        id: imageViewer
                        source: images.getCurrentImage()
                        Layout.preferredWidth: parent.width - 200
                        Layout.preferredHeight: parent.height
                    }
                    Button{
                        text: "Next"
                        Layout.preferredWidth: 100
                        Layout.preferredHeight: 100
                        onClicked:  imageViewer.source = images.next()
                    }
                }
            }
        }
        Tab{
            id: secondTab
            title: "Third"
            GridLayout{
                rows: 2
                columns: 2
                Button{
                    text: "Light"
                    onClicked: clickIndexSignal(0)
                }
                Button{
                    text: "Dark"
                    onClicked: clickIndexSignal(1)
                }
                Button{
                    text: "Save app state"
                }
                Button{
                    text: "Reset to default"
                    onClicked: resetToDefaults()
                }
            }
        }
    }

    Dialog {
        id: dateDialog
        objectName: "dialog"
        signal addRecord(string first, string second,
                         string third, string fourth, string fifth,
                         string sixth, string seventh)
        visible: false
        title: "Choose a date"
        standardButtons: StandardButton.Ok | StandardButton.Cancel

        onAccepted:
        {
            if (firstTextField.text.length !== 0 &&
                  secondTextField.text.length!== 0 &&
                  thirdTextField.text.length !== 0 &&
                  fourthTextField.text.length!== 0 &&
                  fifthTextField.text.length!== 0 &&
                  sixthTextField.text.length!== 0 &&
                  seventhTextField.text.length !== 0  )
            {
                addRecord(firstTextField.text,
                                  secondTextField.text,
                                  thirdTextField.text,
                                  fourthTextField.text,
                                  fifthTextField.text,
                                  sixthTextField.text,
                                  seventhTextField.text)
                firstTextField.text = ""
                secondTextField.text = ""
                thirdTextField.text = ""
                fourthTextField.text = ""
                fifthTextField.text = ""
                sixthTextField.text = ""
                seventhTextField.text = ""
            }
            else
            {
                var popupComponent = Qt.createComponent("qrc:/Popup.qml")
                var popup2 = popupComponent.createObject(app, {"parent" : app});
                popup2.open()
            }
        }

        GridLayout
        {
            columns: 2
            Label{
                y: firstTextField.y
                width: 30
                height: 30
                text: "First"
            }
            TextField {
                id: firstTextField
                width: 30
                height: 30
                validator: IntValidator {bottom: 0; top: 255;}
            }
            Label{
                width: 30
                height: 30
                text: "Second"
            }
            TextField{
                id: secondTextField
                width: 30
                height: 30
                validator: DoubleValidator {bottom: 0; top: 255;}
            }
            Label{
                width: 30
                height: 30
                text: "Third"
            }
            TextField {
                id: thirdTextField
                width: 30
                height: 30
            }
            Label{
                width: 30
                height: 30
                text: "Fourth"
            }
            TextField{
                id: fourthTextField
                width: 30
                height: 30
                validator: IntValidator {bottom: 0; top: 255;}
            }
            Label{
                width: 30
                height: 30
                text: "Fifth"
            }
            TextField{
                id: fifthTextField
                width: 30
                height: 30
                validator: DoubleValidator {bottom: 0; top: 255;}
            }
            Label{
                width: 30
                height: 30
                text: "Sixth"
            }
            TextField{
                id: sixthTextField
                width: 30
                height: 30
            }
            Label{
                width: 30
                height: 30
                text: "Seventh"
            }
            TextField{
                id: seventhTextField
                width: 30
                height: 30
            }
        }
    }
}

