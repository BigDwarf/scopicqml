#include "imageholder.h"

ImageHolder::ImageHolder(QSettings *settings):
    mCurrentImage(0),
    mSettings(settings)
{
    mCurrentImage = mSettings->value("CurrentImageIndex", -1).toInt();
    int imageCount = mSettings->value("ImagesCount").toInt();
    for (int i = 0; i < imageCount; ++i)
    {
        mImages << mSettings->value("Images" + QString::number(i)).toString();
    }
}

ImageHolder::~ImageHolder()
{
    mSettings->setValue("CurrentImageIndex", mCurrentImage);
    mSettings->setValue("ImagesCount", mImages.size());
    for(int i = 0; i < mImages.size(); ++i)
    {
        mSettings->setValue("Images" + QString::number(i), mImages[i]);
    }
}

QString ImageHolder::addImage(QString path)
{
    if (mCurrentImage < 0)
        mCurrentImage = 0;
    mImages.insert(mCurrentImage, path);
    return getCurrentImage();
}

QString ImageHolder::getCurrentImage()
{
    if (mCurrentImage < 0)
    {
        return "";
    }
    return mImages[mCurrentImage];
}

QString ImageHolder::next()
{
    if (mImages.size() == 0)
    {
        return QString();
    }
    if (mCurrentImage + 1 >= mImages.size())
    {
        mCurrentImage = 0;
    }
    else
    {
        mCurrentImage++;
    }
    return mImages[mCurrentImage];
}

QString ImageHolder::previous()
{
    if (mImages.size() == 0)
    {
        return QString();
    }

    if (mCurrentImage == 0)
    {
        mCurrentImage = mImages.size()-1;
    }
    else
    {
        mCurrentImage--;
    }
    return mImages[mCurrentImage];
}

QString ImageHolder::removeImage()
{
    mImages.removeAt(mCurrentImage);
    return previous();
}
