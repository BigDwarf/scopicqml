#ifndef IMAGEHOLDER_H
#define IMAGEHOLDER_H

#include <QObject>
#include <QList>
#include <QString>
#include <QSettings>

class ImageHolder : public QObject
{
    Q_OBJECT
public:
    ImageHolder(QSettings *settings);
    ~ImageHolder();
    Q_INVOKABLE QString addImage(QString path);
    Q_INVOKABLE QString getCurrentImage() ;
    Q_INVOKABLE QString next();
    Q_INVOKABLE QString previous();
    Q_INVOKABLE QString removeImage();
private:
    QList<QString> mImages;
    int mCurrentImage;
    QSettings *mSettings;
};

Q_DECLARE_METATYPE(QList<QString>)

#endif // IMAGEHOLDER_H
