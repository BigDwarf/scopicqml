#ifndef FIRSTTABMODEL_H
#define FIRSTTABMODEL_H

#include <QObject>
#include <QAbstractTableModel>
#include <QSortFilterProxyModel>
#include <QString>
#include <QSettings>
#include <QDataStream>

enum Roles
{
    FIRST = Qt::UserRole,
    SECOND,
    THIRD,
    FOURTH,
    FIFTH,
    SIXTH,
    SEVENTH,
    ROLES_COUNT
};
struct Data
{

   int valueInt;
   double valueDouble;
   QString valueStr;
   int valueInt_2;
   double valueDouble_2;
   QString valueStr_2;
   QString valueStr_3;
private:
    friend QDataStream & operator << (QDataStream &, const Data &);
    friend QDataStream & operator >> (QDataStream &, Data &);
};

Q_DECLARE_METATYPE(Data)
QDataStream& operator<<(QDataStream& out, const Data& v);
QDataStream& operator>>(QDataStream& in, Data& v);

class FirstTabModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit FirstTabModel(QSettings *settings);//MyData *the_data);
    ~FirstTabModel();
    int rowCount(const QModelIndex & parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
public slots:
    void addData(const QString &first, const QString &second, const QString &third,
                 const QString &fourth, const QString &fifth, const QString &sixth,
                 const QString &seventh);
signals:

private:
   QList<Data> mData;
   QSettings *mSettings;
};

#endif // FIRSTTABMODEL_H
